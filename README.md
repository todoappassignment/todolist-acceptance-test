# TodoList Acceptance Test
TodoList Acceptance Test Project.

## Requirements
- [npm](https://www.npmjs.com/)

## Features
* Add new todo
* Delete todo from the list
* Complete/Incomplete todo

## Tech Stack
* Vue: 2.6.11
* Axios: 0.19.2
* Vue Test Utils: 1.0.0-beta.31
* Vue Unit Test Jest Plugin: 4.3.0

## Project Structure

```
.
├── cypress
|   ├── integration        # Acceptance tests                   
├── Dockerfile             # Dockerfile to run tests in a Docker container via CI/CD tool
├── .gitlab-ci.yml         # Pipeline file
├── README.md
├── package.json
```
## Build and Tests
### Building the project

Project setup
```
$ npm install
```

### Running tests 

Run your tests on chrome
```
$ npm run cypress:run:chrome 
```

Run your tests on electron
```
$ npm run cypress:run
```

## Deploy Project 
[Gitlab CI/CD pipeline](.gitlab-ci.yml) Test stage to run acceptance tests.