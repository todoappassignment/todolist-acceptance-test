describe('Todo List', () => {
    const BASE_URL = Cypress.env('BASE_URL')
    const API_BASE_URL = Cypress.env('API_BASE_URL')
    beforeEach(() => {
        cy.request('GET', API_BASE_URL)
            .then(response => {
                let todoList = response.body
                for (index in todoList) {
                    let todo = todoList[index]
                    cy.request('DELETE', API_BASE_URL + todo.id)
                }
            })
    })

    it('should add "buy some milk" to the empty todo list', function () {
        //given
        cy.visit(BASE_URL)
        let todo = 'buy some milk'

        //when
        cy
            .get('#new-todo-input')
            .type(todo)
            .get('#add-todo-btn')
            .click()
        //then
        cy
            .get('#todo-items')
            .children('.todo-item')
            .should('have.length', 1)
            .get('.todo-item-label')
            .should('have.text', todo)
    })

    it('should add "enjoy the assignment" to the todo list', function () {
        //given
        let existingTodo = {
            'title': 'buy some milk'
        }
        cy.request('POST', API_BASE_URL, existingTodo)
        cy.visit(BASE_URL)

        let todo = 'enjoy the assignment'

        //when
        cy
            .get('#new-todo-input')
            .type(todo)
            .get('#add-todo-btn')
            .click()

        //then
        let todos = cy
            .get('#todo-items')
            .children('.todo-item');

        todos.should('have.length', 2)
        todos.get('.todo-item-label').eq(0).should('have.text', 'buy some milk')
        todos.get('.todo-item-label').eq(1).should('have.text', todo)
    });

    it('should complete "buy some milk" todo', function () {
        //given
        let existingTodo = {
            'title': 'buy some milk'
        }
        cy.request('POST', API_BASE_URL, existingTodo)
        cy.visit(BASE_URL)

        //when
        let todo = cy.get('#todo-items')
            .children('.todo-item')
            .get('.todo-item-label').eq(0).click()

        //then
        todo.should('have.text', 'buy some milk')
        todo.should('have.class', 'completed')
    });

    it('should incomplete "buy some milk" todo', function () {
        //given
        cy.request('POST', API_BASE_URL, {'title': 'buy some milk'})
            .then(response => {
                cy.request('PUT', API_BASE_URL + response.body.id, {
                    'title': response.body.title,
                    'completed': true
                })
            })

        cy.visit(BASE_URL)

        //when
        let todo = cy
            .get('#todo-items')
            .children('.todo-item')
            .get('.todo-item-label')

        todo.click()

        //then
        todo.should('have.text', 'buy some milk')
            .should('not.have.class', 'completed')
    });

    it('should delete "rest for a while" todo and validate given todo list is empty', function () {
        //given
        cy.request('POST', API_BASE_URL, {'title': 'rest for a while'})
        cy.visit(BASE_URL)

        //when
        cy.get('#todo-items > .todo-item > .remove-todo-item')
            .click()

        //then
        cy.get('#todo-items')
            .children('.todo-item')
            .should('have.length', 0)
    });

    it('should delete "rest for a while" todo and validate given todo list have "drink water"', function () {
        //given
        cy.request('POST', API_BASE_URL, {'title': 'rest for a while'})
        cy.request('POST', API_BASE_URL, {'title': 'drink water'})

        cy.visit(BASE_URL)

        //when
        cy.get('#todo-items')
            .children('.todo-item')
            .find('.todo-item-label')
            .filter(':contains("rest for a while")')
            .parent('.todo-item')
            .children('.remove-todo-item')
            .click()

        //then
        cy.get('#todo-items')
            .children('.todo-item').should('have.length', 1)
            .get('.todo-item-label').eq(0).should('have.text', 'drink water')
    });
})